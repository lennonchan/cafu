The automatically generated "template" header files are created in this
- initially empty - directory.

They are intended to help documentation writers to keep the actual header
files in `src/` up-to-date (e.g. by using a tool like BeyondCompare for
synchronization).

The actual scripting reference documentation is created by Doxygen from the
contents of `src/`.
