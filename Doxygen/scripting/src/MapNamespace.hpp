/**
 * This "Map / Entity" group contains all script classes for the \emph{DeathMatch} example game that ships with Cafu.
 *
 * Entities are the "living" objects in a game map, such as the human players, computer controlled monsters,
 * but also doors, lifts, light sources, and many more. Some of their functionality is implemented in C++,
 * but a good part is also available for use and customization from map scripts.
 * Map scripts are used to program the map-specific logic of the entities that live within it,
 * and the details of the game play.
 *
 * For example map scripts, see the @c *.lua files in
 *   - http://trac.cafu.de/browser/cafu/trunk/Games/DeathMatch/Worlds
 *
 * Map scripts have access to our <a href="group__Common.html">Common</a> libraries as well as
 * to (most of) the <a href="http://www.lua.org/manual/5.1/manual.html#5">Lua standard libraries</a>.
 */
namespace Map { }
